import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Encoder {
    private String text;
    private PriorityQueue<Node> pq = new PriorityQueue<>();
    private Tree tree;

    public Encoder(String text){
        this.text = text;

        Map<Character, Integer> charMap = new HashMap<>();
        //Adding all the letters from the text to map with frequency
        for(char x : text.toCharArray()){
            charMap.put(x,charMap.getOrDefault(x,0)+1);
        }

        //Adding all the letters in the PriorityQueue
        for (Map.Entry<Character, Integer> entry : charMap.entrySet()) {
            pq.add(new Node(Character.toString(entry.getKey()),entry.getValue()));
        }

        //Generating node from PriorityQueue
        Node node1;
        Node node2;
        while (pq.size() > 1) {
            node1 = pq.poll();
            node2 = pq.poll();
            pq.add(new Node(node1.getData() + node2.getData(),node1,node2,node1.getWeight() + node2.getWeight()));
        }

        // Generating tree from PriorityQueue node
        tree = new Tree(pq.poll());
    }

    public String encodedString(){
        StringBuilder codeString = new StringBuilder();
        //Appending the encoded letter in one string builder
        for (char x : text.toCharArray()) {
            codeString.append(tree.encodeLetter(Character.toString(x)));
        }

        return codeString.toString();
    }

    public int encodedTextLength(){
        //Calculates the length of the encoded string
        return encodedString().length();
    }

    public int treeHeight(){
        //returns the height of the tree
       return tree.size(tree.getRoot()) - 1;
    }
}
