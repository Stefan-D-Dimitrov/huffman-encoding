public class Tree {
    private Node root;

    public Tree(Node node){
        root = node;
    }

    public Node getRoot(){
        return root;
    }

    public String encodeLetter(String s){
        StringBuilder x = new StringBuilder();

        Node node = root;
        while(node != null){
            if(node.getLeft().getData().contains(s)){
                x.append(0);
                node = node.getLeft();
            } else if(node.getRight().getData().contains(s)){
                x.append(1);
                node = node.getRight();
            }

            if(node.getLeft() == null && node.getRight() == null){
                break;
            }
        }

        return x.toString();
    }

    int size(Node node)
    {
        if (node == null)
            return 0;
        else
            return(Math.max(size(node.getLeft()),size(node.getRight()))+1);
    }
}
