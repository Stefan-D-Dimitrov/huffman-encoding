public class Node implements Comparable<Node>{
    private String data;
    private Node left;
    private Node right;
    private int weight;

    public Node(String data, int weight){
        this.data = data;
        this.weight = weight;
    }

    public Node(String data,Node left, Node right, int weight){
        this.data = data;
        this.left = left;
        this.right = right;
        this.weight = weight;
    }

    public String getData() {
        return data;
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public int compareTo(Node o) {
        return Integer.compare(weight,o.getWeight());
    }
}
